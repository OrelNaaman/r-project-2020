setwd("C:/Users/Orel Naaman/Desktop/������ R/rapp_project")
#read the data from file
original.data <- read.csv("androidApps.csv", row.names = 1, stringsAsFactors = FALSE)
#copy data 
data.pre <- original.data
#starting to undarstand the data
summary(data.pre)
str(data.pre)
head(data.pre)

str(data.pre$numberreviews)

#Data preparation and EDA 
?gsub
#we want to change numberreviews to int 
data.pre$numberreviews<- as.integer(gsub(",","",data.pre$numberreviews))
review.num.avg <- mean(data.pre$numberreviews, na.rm = TRUE) 
review.num.sd <- sd(data.pre$numberreviews, na.rm = TRUE) 
length(data.pre$numberreviews[data.pre$numberreviews < 35])

#check if we can do somthing with this col to Complete missing data
summary(as.factor(data.pre$one[is.na(data.pre$numberreviews)]))
summary(as.factor(data.pre$two[is.na(data.pre$numberreviews)]))
summary(as.factor(data.pre$three[is.na(data.pre$numberreviews)]))

data.pre <- data.pre[data.pre$numberreviews > 35,]
data.pre <- data.pre[ !is.na(data.pre$numberreviews),]
data.pre <- data.pre[ !is.na(data.pre$rating),]
data.pre <- data.pre[ data.pre$rating < 5,]
#avg raiting for app
data.pre$rating <- as.numeric(data.pre$rating)
avg_rait <- mean(as.numeric(data.pre$rating))

summary(data.pre$rating)
#check that each category have enough data
length(data.pre$rating[data.pre$rating > 4.1])
length(data.pre$rating[data.pre$rating <= 4.1])


#building target
category_classification <- function(x){
  if(x > 4.1){
    return('good')
  }
  else{
    return('bad')
  }
}
summary(data.pre$rating)
data.pre$rating <- sapply(data.pre$rating, category_classification)
data.pre$rating <- as.factor(data.pre$rating)
summary(data.pre$rating)
#install.packages('ggplot2')
library(ggplot2)
ggplot(data.pre, aes(rating)) + geom_bar()

#remove one, two, three, four, five col
data.pre <- data.pre[,c(1:27)]

#Data preparation 
summary(data.pre)

summary(as.factor(data.pre$fancyname))
data.pre$fancyname <- NULL

summary(as.factor(data.pre$company))

#Extra info is a text that include some details that we can extract from other columns
summary(as.factor(data.pre$extrainfo))
data.pre$extrainfo <- gsub("[][?!#$%()*,.:;<=>@^_|~.{}]"," ", data.pre$extrainfo)
data.pre$extrainfo <- gsub("\\p{P}", "", data.pre$extrainfo, perl=TRUE)
head(data.pre$extrainfo)

#install.packages("stopwords")

#install.packages('tm')
library(tm)
?stopwords
data.pre$extrainfo <- gsub("[][?!#$%()*,.:;<=>@^_|~.{}]"," ", data.pre$extrainfo)
data.pre$extrainfo <- removeWords(data.pre$extrainfo, stopwords(kind = "en"))
#install.packages('wordcloud')
library(wordcloud)
#color pallet 
pal <- brewer.pal(8,'Dark2')

wordcloud(data.pre$extrainfo, min.freq = 1000, random.order = TRUE, colors = pal)
wordcloud(data.pre$extrainfo[data.pre$rating == 'good'], min.freq = 1000, random.order = TRUE, colors = pal)$freqTable 
wordcloud(data.pre$extrainfo[data.pre$rating == 'bad'], min.freq = 1000, random.order = TRUE, colors = pal)
#i have this details in specific way in other columns

data.pre$extrainfo <- NULL


#source and systemApp have only one kind so i can remove it
data.pre$source <- NULL
data.pre$SystemApp <-NULL
#almost all is NA we can remove secondprice col
data.pre$secondprice <- NULL

#data that ready to change to factor
summary(data.pre)
data.pre$category <- as.factor(data.pre$category)
data.pre$company <- as.factor(data.pre$company)
data.pre$source <- as.factor(data.pre$source)
data.pre$age_rating <- as.factor(data.pre$age_rating)
data.pre$basename <- as.factor(data.pre$basename)
#categorygame factor already clean if its not a game it wrote "no game"
data.pre$categorygame <- as.factor(data.pre$categorygame)


#in this data we have alot of factor that need to check that ok...
#we check each one with the commands head() str() summary() summary(as.factor()) and more
#the easy factors that only need change name and to factor we concentrated here


#functions for changing data to factor with clear name
purchases_allow <- function(x){
  ifelse(x == 1, return('allow purchases'), return('not allowed'))
}


contain_ads <- function(x){
  if(x == 1){
    return('contain ads')
  }
  if(x == 0){
    return('no ads')
  }
}
share_info <- function(x){
  if(x == 1){
    return('have third party cookies')
  }
  if(x == 0){
    return('not share info')
  }
}
user_location <- function(x){
  if(x == 1){
    return('share user location')
  }
  if(x == 0){
    return('not share location')
  }
}
unrestrict_internet <- function(x){
  if(x == 1){
    return('unrestricted internet authorization?')
  }
  if(x == 0){
    return('limit internet')
  }
}
users_interact<- function(x){
  if(x == 1){
    return('multiplayer')
  }
  if(x == 0){
    return('no intract with other user')
  }
}

is_game<- function(x){
  if(x == 1){
    return('mobile game')
  }
  if(x == 0){
    return('not a game')
  }
}

parental_guidance<- function(x){
  if(x == 1){
    return('parental guidance recommended')
  }
  if(x == 0){
    return('not needed prental guidance')
  }
}


fuzzy_match<- function(x){
  if(x == 1){
    return('Fuzzy matching (computer assisted translation)')
  }
  if(x == 0){
    return('other (not fuzzy)')
  }
}
paid_app<- function(x){
  if(x == 1){
    return('paid app')
  }
  if(x == 0){
    return('free')
  }
}

#change the factors levels name and change to factor (from char)
data.pre$purchases <- as.factor(sapply(data.pre$purchases, purchases_allow))
data.pre$ads <- as.factor(sapply(data.pre$ads, contain_ads))
data.pre$sharesinfo <- as.factor(sapply(data.pre$sharesinfo, share_info))
data.pre$shareslocation <- as.factor(sapply(data.pre$shareslocation, user_location))
data.pre$unrestrictedinternet <- as.factor(sapply(data.pre$unrestrictedinternet, unrestrict_internet))
data.pre$usersinteract <- as.factor(sapply(data.pre$usersinteract, users_interact))
data.pre$game <- as.factor(sapply(data.pre$game, is_game))
data.pre$Parentalguidance <- as.factor(sapply(data.pre$Parentalguidance , parental_guidance))
data.pre$FuzzyMatched <- as.factor(sapply(data.pre$FuzzyMatched, fuzzy_match))
data.pre$paidapp <- as.factor(sapply(data.pre$paidapp, paid_app))

#new category need to be factor we will deal with him down 
data.pre$New.category <- as.factor(data.pre$New.category)

#check data.pre$paidapp propriety (currect data)
summary(data.pre$paidapp[data.pre$price == "free"])

#We will make a new col that say bad categorize 
vektor_term_categorize <- as.character(data.pre$category) == as.character(data.pre$New.category)
data.pre$bad_categorize <- as.factor(ifelse(vektor_term_categorize, 'same' , 'bad categorize'))
summary(data.pre$bad_categorize)

ggplot(data.pre,aes(bad_categorize)) + geom_bar(aes(fill = rating))
ggplot(data.pre) + aes(x = bad_categorize, fill = rating) +geom_bar(position = "fill")
#there is a diffrence
#have a meaning
ggplot(data.pre,aes(New.category)) + geom_bar(aes(fill = rating))
ggplot(data.pre) + aes(x = New.category, fill = rating) +geom_bar(position = "fill")


data.pre$updated_category <- data.pre$New.category
data.pre$New.category <- NULL
data.pre$category <- NULL

#we have too many category
ggplot(data.pre,aes(updated_category)) + geom_bar(aes(fill = rating))
ggplot(data.pre) + aes(x = updated_category, fill = rating) +geom_bar(position = "fill")


find_names_of_factor_by_freq <- function(vektor, freq){
  vek <- names(summary(vektor)[summary(vektor) > freq])
  return(vek[vek != "(Other)"]) 
}

swith_to_other_small_category <- function(x, factor_to_keep){
  if(as.character(x) %in% as.character(factor_to_keep)){
    return(as.character(x))
  }
  else{
    return('other')
  }
}

#remove levels with low freq and check if the new category have influence on target
summary(data.pre$updated_category)
name.of.vektor <- find_names_of_factor_by_freq(data.pre$updated_category, 100)
data.pre$updated_category <- sapply(data.pre$updated_category, swith_to_other_small_category, name.of.vektor )
data.pre$updated_category <- as.factor(data.pre$updated_category)
summary(data.pre$updated_category)
ggplot(data.pre) + aes(x = updated_category, fill = rating) +geom_bar(position = "fill")


summary(data.pre$company)
name.of.vektor <- find_names_of_factor_by_freq(data.pre$company, 30)
data.pre$company <- sapply(data.pre$company, swith_to_other_small_category, name.of.vektor )
data.pre$company <- as.factor(data.pre$company)
summary(data.pre$company)
ggplot(data.pre) + aes(x = company, fill = rating) +geom_bar(position = "fill")




summary(data.pre$basename)
name.of.vektor <- find_names_of_factor_by_freq(data.pre$basename, 40)
data.pre$basename <- sapply(data.pre$basename, swith_to_other_small_category, name.of.vektor )
data.pre$basename <- as.factor(data.pre$basename)
ggplot(data.pre) + aes(x = basename, fill = rating) +geom_bar(position = "fill")


summary(data.pre$categorygame)
name.of.vektor <- find_names_of_factor_by_freq(data.pre$categorygame, 40)
data.pre$categorygame <- sapply(data.pre$categorygame, swith_to_other_small_category, name.of.vektor )
data.pre$categorygame <- as.factor(data.pre$categorygame)
summary(data.pre$categorygame)
ggplot(data.pre) + aes(x = categorygame, fill = rating) +geom_bar(position = "fill")


str(data.pre)
ggplot(data.pre,aes(company)) + geom_bar(aes(fill = rating))
ggplot(data.pre) + aes(x = company, fill = rating) +geom_bar(position = "fill")


#numeric column analysis
data.pre$low_price_item <-  as.numeric(data.pre$low_price_item)
data.pre$top_price_item <-  as.numeric(data.pre$top_price_item)

ggplot(data.pre, aes(rating,low_price_item)) + geom_boxplot() + ylim(0,10)

ggplot(data.pre, aes(rating,top_price_item)) + geom_boxplot() + ylim(0,300)

deal_with_NA <- function(x, avg){
  if(is.na(x)){
    return(avg)
  }
  else{
    return(x)
  }
}


avg_low_price <- mean(data.pre$low_price_item , na.rm = TRUE)
avg_top_price <- mean(data.pre$top_price_item, na.rm = TRUE)

data.pre$low_price_item <- sapply(data.pre$low_price_item, deal_with_NA,avg_low_price)
data.pre$top_price_item <- sapply(data.pre$top_price_item, deal_with_NA,avg_top_price)
ggplot(data.pre, aes(rating,low_price_item)) + geom_boxplot() + ylim(0,10)
ggplot(data.pre, aes(rating,top_price_item)) + geom_boxplot()+ ylim(0,25)
#we dont see some behavior to categorize just a bit we think range col will do better job
#we saw that this both col have some influence so we will keep one of them
#we will keep low_price_item (we can one of them so we choose him becouse its look he have more effect on target)
ggplot(data.pre, aes(low_price_item,top_price_item)) + geom_bin2d(aes(fill = rating), binwidth = c(20,20))

#Building new better col
data.pre$range_price <- data.pre$top_price_item - data.pre$low_price_item
ggplot(data.pre, aes(rating,range_price)) + geom_boxplot() + ylim(0, 100)
data.pre$top_price_item <- NULL
summary(data.pre)
str()

ggplot(data.pre) +aes(x = company, fill = rating) + geom_bar(position = "fill")

ggplot(data.pre, aes(rating,Downloads)) + geom_boxplot() +ylim(0,1200000)
ggplot(data.pre) +aes(x = as.factor(Downloads), fill = rating) + geom_bar(position = "fill")
str(data.pre$Downloads)


deal_with_price <- function(x){
  if(is.na(x)){
    return(NA)
  }
  if(x == "free"){
    return(0)
  }
  else{
    return(as.numeric(x))
  }
}
priceDealNA <- function(x, avg){
  if(is.na(x)){
    return(avg)
  }
  else{
    return(x)
  }
}
data.pre$price <- sapply(data.pre$price , deal_with_price)
data.pre$price <-sapply(data.pre$price, priceDealNA, mean(data.pre$price, na.rm = TRUE))
ggplot(data.pre, aes(price, fill = rating)) + geom_histogram(position = 'fill')
ggplot(data.pre) +aes(x = as.factor(round(price)), fill = rating) + geom_bar(position = "fill")
ggplot(data.pre) +aes(x = paidapp, fill = rating) + geom_bar(position = "fill")
data.pre$price<- NULL
summary(data.pre)

#building the models
#install.packages('caTools')
#split the data to training and test set
library(caTools) 
set.seed(101)
filter <- sample.split(data.pre$company , SplitRatio = 0.7)
data.train <- subset(data.pre, filter ==T)
data.test <- subset(data.pre, filter ==F)

summary(data.pre)
dim(data.train)
#logistic regression
app.mode.logistic.regression <- glm(rating ~ ., family = binomial(link = 'logit'), data = data.train)

str(data.test$rating)
predicted.logistic.test <- predict(app.mode.logistic.regression, newdata = data.test, type = 'response')
#predict good (the first argument is fail (the first level of the factor) and we predict success)
summary(predicted.logistic.test)

confusion_matrix_logistit_regression <- table(predicted.logistic.test > 0.5, data.test$rating )
str(weather.test)

precision_regression <- confusion_matrix_logistit_regression['FALSE','bad']/(confusion_matrix_logistit_regression['FALSE','bad'] + confusion_matrix_logistit_regression['FALSE','good'])
recall_regression <- confusion_matrix_logistit_regression['FALSE','bad']/(confusion_matrix_logistit_regression['FALSE','bad'] + confusion_matrix_logistit_regression['TRUE','bad'])


#baise naive
#install.packages('e1071')
library(e1071)
summary(data.train[,-17])
dim(data.train)
model.bayse <- naiveBayes(data.train[,-17],data.train$rating)
#prediction
prediction_bayse <- predict(model.bayse,data.test[,-17], type = 'raw')
str(prediction_bayse)
prediction_good <- prediction_bayse[,'good']
actual <- data.test$rating
predicted_bayse <- prediction_good > 0.5
#so true its where good get more then 0.65 prob 

#confusion matrix 
conf_matrix_bayse <- table(predicted_bayse, actual)

#precision and recall 
precision_bayse <- conf_matrix_bayse['FALSE','bad']/(conf_matrix_bayse['FALSE','bad'] + conf_matrix_bayse['FALSE','good'])
recall_bayse <- conf_matrix_bayse['FALSE','bad']/(conf_matrix_bayse['FALSE','bad'] + conf_matrix_bayse['TRUE','bad'])



#desicion tree
#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)

tree.model <- rpart(rating~.,data.train)
#print the tree
rpart.plot(tree.model, box.palette = 'RdBu', shadow.col = 'grey', nn = TRUE)

prediction.prob.tree <- predict(tree.model,data.test)
summary(prediction.prob.tree)
summary(prediction.prob.tree)
prediction_tree <- prediction.prob.tree[,'good'] > 0.6
##check
conf.mat.tree <- table(prediction_tree , actual )
precision_tree <- conf.mat.tree['FALSE','bad']/(conf.mat.tree['FALSE','bad'] + conf.mat.tree['FALSE','good'])
recall_tree <- conf.mat.tree['FALSE','bad']/(conf.mat.tree['FALSE','bad'] + conf.mat.tree['TRUE','bad'])

#Inside who really bought how many we said 

#random forest
#install.packages('randomForest')
library(randomForest)
RF.model <- randomForest(rating~.,data.train, importance = TRUE)
predict.prob.RF <- predict(RF.model,data.test, type = "prob")
summary(predict.prob.RF)
prediction.RF <- predict.prob.RF[,'good'] > 0.5
conf.mat.RF <- table(prediction.RF,actual)

presicion.RF <- conf.mat.RF['FALSE','bad']/(conf.mat.RF['FALSE','bad'] + conf.mat.RF['FALSE','good'])
recall.RF <- conf.mat.RF['FALSE','bad']/(conf.mat.RF['FALSE','bad'] + conf.mat.RF['TRUE','bad'])


#ROC chart 
#install.packages('pROC')
library(pROC)
#he need the prob becaouse he choose the desicion boundry
rocT <- roc(data.test$rating, prediction.prob.tree[,'good'], direction = "<", levels = c('bad','good'))
rocRF <- roc(data.test$rating,predict.prob.RF[,'good'], direction = "<", levels = c('bad','good')) 
rocBayse <- roc(data.test$rating,prediction_bayse[,'good'], direction = "<", levels = c('bad','good')) 
rocLogistit <- roc(data.test$rating,predicted.logistic.test, direction = "<", levels = c('bad','good')) 
 
par(mfrow = c(1,1))
plot(rocT, col = 'red', main = 'ROC chart')
par(new=TRUE)
plot(rocRF, col = 'blue', main = 'ROC chart')
par(new=TRUE)
plot(rocBayse, col = 'purple', main = 'ROC chart')
par(new=TRUE)
plot(rocLogistit, col = 'green', main = 'ROC chart')
#random forest better for each boundry value

#economic model
#we will check it with cost function
compute.cost <- function(bl,cost1,cost2, confusion_matrix, predicted){
  confusion_matrix <- matrix(confusion_matrix,nrow = 2,ncol = 2)
  confusion_matrix <- table(data.test$rating, predicted >bl)
  cost <- confusion_matrix[1,2]*cost1 + confusion_matrix[2,1]*cost2
  return (cost)
}

bl.vec <- seq(0,1,length.out = 21)

#remove 0 and 0.95 and 1 
bl.vec <- bl.vec[2:19]

#Comnpute optimum 
results <- sapply(bl.vec, compute.cost, cost1 = 70, cost2 =30 , confusion_matrix_logistit_regression, predicted.logistic.test)
plot(bl.vec, results)

#A general function to compute optimum 
findmin <- function(FUN, vec, cost1, cost2, confusion_matrix, predicted){
  min = 10000000000
  for (item in vec){
    if (FUN(item, cost1, cost2, confusion_matrix, predicted)<min) {
      min <- FUN(item, cost1, cost2,confusion_matrix, predicted )
      bestitem <- item
    }  
  }
  return (c(bestitem,min))
}

logistic.regression.findmin.result <- findmin(compute.cost,bl.vec, 200, 500 ,confusion_matrix_logistit_regression , predicted.logistic.test)
RF.findmin.result <- findmin(compute.cost,bl.vec, 200, 500 ,conf.mat.RF , predict.prob.RF[,'good'])
RF.findmin.result[2] < logistic.regression.findmin.result[2]
RF.findmin.result[1]

prediction.RF_real_boundry <- predict.prob.RF[,'good'] > 0.15
conf.mat.RF_real_boundry <- table(prediction.RF_real_boundry,actual)

presicion_done <- conf.mat.RF['FALSE','bad']/(conf.mat.RF['FALSE','bad'] + conf.mat.RF['FALSE','good'])
recall_done <- conf.mat.RF['FALSE','bad']/(conf.mat.RF['FALSE','bad'] + conf.mat.RF['TRUE','bad'])

#need to check with ROI What the income from ['TRUE','good'] and what the income from ['FALSE','bad']
